﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBenhVien.Model
{
    class Admin
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string hashpass { get; set; }
        public string adminName { get; set; }

        public Admin(int id, string userName, string hashpass, string adminName)
        {
            this.id = id;
            this.userName = userName;
            this.hashpass = hashpass;
            this.adminName = adminName;
        }
        public Admin() {  }
        public Admin(DataRow row)
        {
            this.id = (int)row["id"];
            this.userName = (string)row["userName"];
            this.hashpass = (string)row["hashpass"];
            this.adminName = (string)row["adminName"];
        }
    }
}

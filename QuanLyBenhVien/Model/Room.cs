﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBenhVien.Model
{
    class Room
    {
        public int number { get; set; }
        public string name { get; set; }
        public int numberOfBed { get; set; }
         

        public Room(int number, string name, int numberOfBed)
        {
            this.number = number;
            this.numberOfBed = numberOfBed;
            this.name = name;
        }
        public Room() { }
        public Room(DataRow row)
        {
            this.number = (int)row["number"];
            this.name = (string)row["name"];
            this.numberOfBed = (int)row["numberOfBed"];
        }
    }
}

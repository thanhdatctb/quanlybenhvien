﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBenhVien.Model
{
    class Resume
    {
        public int id { get; set; }
        public int patientId { get; set; }
        public int roomId { get; set; }
        public int doctorId { get; set; }
        public DateTime meetingDate { get; set; }

        public Resume(int id, int patientId, int roomId, int doctorId, DateTime meetingDate)
        {
            this.id = id;
            this.patientId = patientId;
            this.roomId = roomId;
            this.doctorId = doctorId;
            this.meetingDate = meetingDate;
        }

        public Resume()
        {
        }
        public Resume(DataRow row)
        {
            this.id = (int)row["id"];
            this.patientId = (int)row["patientId"];
            this.roomId = (int)row["roomId"];
            this.doctorId = (int)row["doctorId"];
            this.meetingDate = (DateTime)row["meetingDate"];
        }
    }
}

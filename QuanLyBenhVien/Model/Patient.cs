﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBenhVien.Model
{
    public class Patient
    {
        public int id { get; set; }
        public string name { get; set; }
        public int age { get; set; }
        public string tell { get; set; }
        public int status { get; set; }
        public string username { get; set; }
        public string hashpass { get; set; }
        public Patient(int id, string name, int age, string tell, int status)
        {
            this.id = id;
            this.name = name;
            this.age = age;
            this.tell = tell;
            this.status = status;
        }

        public Patient(string name, int age, string tell, int status, string username, string hashpass, int id = 0) : this(id, name, age, tell, status)
        {
            this.username = username;
            this.hashpass = hashpass;
        }

        public Patient() { }
        public Patient(DataRow row)
        {
            this.id = (int)row["id"];
            this.name = (string)row["name"];
            this.age = (int)row["age"];
            this.tell = (string)row["tell"];
            this.status = (int)row["status"];
            this.username = (string)row["username"];
            this.hashpass = (string)row["hashpass"];
        }
    }
}

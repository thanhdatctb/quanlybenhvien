﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBenhVien.Model
{
    public class Doctor
    {
        public int id { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public string major { get; set; }
        public string username { get; set; }
        public string hashpass { get; set; }

        public Doctor(int id, string name, string role, string major)
        {
            this.id = id;
            this.name = name;
            this.role = role;
            this.major = major;
        }

        public Doctor(string name, string role, string major, string username, string hashpass, int id = 0) : this(id, name, role, major)
        {
            this.username = username;
            this.hashpass = hashpass;
        }

        public Doctor() { }
        public Doctor(DataRow row)
        {
            this.id = (int)row["id"];
            this.name = (string)row["name"];
            this.role = (string)row["role"];
            this.major = (string)row["major"];
            this.username = (string)row["username"];
            this.hashpass = (string)row["hashpass"];
        }
    }
}

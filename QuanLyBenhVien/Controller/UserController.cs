﻿
using QuanLyBenhVien.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBenhVien.Controller
{
    abstract class UserController
    {
        public int? LogIn(String username, string password, string table)
        {
            string hashPassword = PasswordController.Sha1Hash(password);//Tạo ra hashpass bằng cách băm password bằng thuật toán sha1
            String sql = "select id from " + table + " where username = @username and hashpass = @hashpass";//tạo ra câu lệnh sql chọn id từ bảng trong db vs username = @username và hashpass = @hashpass
            
            var datatable = MyConnection.ExecuteQuery(sql, new object[] { username, hashPassword });
            if (datatable.Rows.Count == 0)
                return null;
            int id = (int)datatable.Rows[0]["id"];
            return id;//Trả về id
        }
    }
}

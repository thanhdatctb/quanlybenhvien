﻿using QuanLyBenhVien.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBenhVien.Controller
{
    class ResumeController
    {
        public List<Resume> GetAll()
        {
            List<Resume> resumes = new List<Resume>();
            string sql = "select * from resume";
            var table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                resumes.Add(new Resume(row));
            }    
            return resumes;
        }
        public bool Add(Resume resume)
        {
            string sql = @"insert into resume (patientId, roomId, doctorId, meetingDate) values ( @patientID , @room , @doctorID , @date )";
            MyConnection.ExecuteNonQuery(sql, new object[] { resume.patientId, resume.roomId, resume.doctorId, resume.meetingDate });
            return true;
        }

        public bool Edit(Resume resume)
        {
            string sql = "update resume set patientId = @patientId , roomId = @roomId , doctorId = @doctorId , meetingDate = @meetingDate where id = @id  ";
            MyConnection.ExecuteNonQuery(sql, new object[] { resume.patientId, resume.roomId, resume.doctorId, resume.meetingDate, resume.id });

            return true;
        }
        public bool Delete(int id)
        {
            string sql = "delete from resume where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] { id });
            return true;
        }

    }
}

﻿using QuanLyBenhVien.Model;
using QuanLyBenhVien.Session;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace QuanLyBenhVien.Controller
{
    class DoctorController: UserController
    {
        public bool LogIn(string username, string password)
        {
            int? id = base.LogIn(username, password, "doctor");
            if(id == null)
            {
                return false;
            }
            MySession.doctor = GetById((int)id);
            return true;
        }
        public Doctor GetById(int id)
        {
            string sql = "select * from doctor where id = @id";
            var table = MyConnection.ExecuteQuery(sql, new object[] { id });
            if(table.Rows.Count == 0)
            {
                return null;
            }
            return new Doctor(table.Rows[0]);
        }
        public List<Doctor> GetAll()
        {
            List<Doctor> doctors = new List<Doctor>();
            string sql = "select * from doctor where status = 1";
            var table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                doctors.Add(new Doctor(row));
            }
            return doctors;
        }
        public bool Add(Doctor doctor)
        {
            string sql = "insert into doctor (name, role, major, username, hashpass) values( @name , @role , @major , @username , @hashpass )";
            string pass = "123";
            string hashPass = PasswordController.Sha1Hash(pass);
            MyConnection.ExecuteNonQuery(sql, new object[] { doctor.name, doctor.role, doctor.major, doctor.username, hashPass });
            return true;
        }
        public bool Edit(Doctor doctor)
        {
            string sql = "update doctor set name = @name , role = @role , major = @major , username = @username where id = @Id";
            MyConnection.ExecuteNonQuery(sql, new object[] { doctor.name, doctor.role, doctor.major, doctor.username, doctor.id });
            return true;
        }
        public bool Delete(int id)
        {
            string sql = "update doctor set status = 0 where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] { id });
            return true;
        }
        public bool UpdatePassword (Doctor doctor)
        {
            string sql = "update doctor set hashpass = @hashpass where id = @id ";
            MyConnection.ExecuteNonQuery(sql, new object[] { doctor.hashpass, doctor.id });
            return true;
        }
        
    }
}

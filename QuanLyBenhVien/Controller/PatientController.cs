﻿using QuanLyBenhVien.Model;
using QuanLyBenhVien.Session;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBenhVien.Controller
{
    class PatientController: UserController
    {
        public bool LogIn(string username, string password)
        {
            int? id = base.LogIn(username, password, "patient");
            if (id == null)
            {
                return false;
            }
            MySession.patient = GetById((int)id);
            return true;
        }
        public Patient GetById(int id)
        {
            string sql = "select * from patient where id = @id";
            var table = MyConnection.ExecuteQuery(sql, new object[] { id });
            if (table.Rows.Count == 0)
            {
                return null;
            }
            return new Patient(table.Rows[0]);
        }
        public List<Patient> GetAll()
        {
            List<Patient> patients = new List<Patient>();
            string sql = "select * from patient where status = 1";
            DataTable table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                patients.Add(new Patient(row));
            }    
            return patients;
        }
        public bool Add(Patient patient)
        {
            string sql = "insert into patient (name, age, tell, status, username, hashpass) values( @name , @age , @tel , 1, @username , @hashpass )";
            string pass = "123";
            string hashPass = PasswordController.Sha1Hash(pass);
            MyConnection.ExecuteNonQuery(sql, new object[] { patient.name, patient.age, patient.tell, patient.username, hashPass });
            return true;
        }
        public bool Edit(Patient patient)
        {
            string sql = "update patient set name = @name , age = @age , tell = @tell , username = @username where id = @Id";
            MyConnection.ExecuteNonQuery(sql, new object[] { patient.name, patient.age, patient.tell, patient.username, patient.id });
            return true;
        }
        public bool Delete(int id)
        {
            string sql = "update patient set status = 0 where id = @Id";
            MyConnection.ExecuteNonQuery(sql, new object[] { id });
            return true
                ;
        }
        public bool UpdatePassword(Patient patient)
        {
            string sql = "update patient set hashpass = @hashpass where id = @Id";
            MyConnection.ExecuteQuery(sql, new object[] { patient.hashpass, patient.id });
            return true;
        }
        public List<Patient> GetByDoctorId(int doctorID)
        {
            string sql = @"select * from patient where id in(
                        select patient.id
                        from resume
                        join doctor on resume.doctorId = doctor.id
                        join patient on resume.patientId = patient.id
                        where doctorId = @doctorID )";
            List<Patient> patients = new List<Patient>();
            DataTable table = MyConnection.ExecuteQuery(sql, new object[] { doctorID });
            foreach(DataRow row in table.Rows)
            {
                patients.Add(new Patient(row));
            }    
            return patients;

        }    
    }
}

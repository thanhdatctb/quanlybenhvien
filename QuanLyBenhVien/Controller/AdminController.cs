﻿using QuanLyBenhVien.Model;
using QuanLyBenhVien.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBenhVien.Controller
{
    class AdminController:UserController
    {
        public bool LogIn(string username, string password)
        {
            int? id = base.LogIn(username, password, "admin");
            if (id == null)
            {
                return false;
            }
            MySession.admin = GetById((int)id);
            return true;
        }
        public Admin GetById(int id)
        {
            string sql = "select * from admin where id = @id";
            var table = MyConnection.ExecuteQuery(sql, new object[] { id });
            if (table.Rows.Count == 0)
            {
                return null;
            }
            return new Admin(table.Rows[0]);
        }
    }
}

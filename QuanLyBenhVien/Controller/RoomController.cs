﻿using QuanLyBenhVien.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBenhVien.Controller
{
    class RoomController
    {
        public List<Room> GetAll()
        {
            List<Room> rooms = new List<Room>();
            string sql = "select * from room";
            var table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                rooms.Add(new Room(row));
            }    
            return rooms;
        }
        public bool Add(Room room)
        {
            string sql = "insert into room values ( @number , @name , @numberOfBed )";
            MyConnection.ExecuteQuery(sql, new object[] { room.number, room.name, room.numberOfBed });
            return true;
        }
        public bool Edit(Room room)
        {
            string sql = "update room set name = @name , numberOfBed = @numberOfBed where number = @number";
            MyConnection.ExecuteQuery(sql, new object[] { room.name, room.numberOfBed, room.number });
            return true;

        }
        public bool Delete (int number)
        {
            string sql = "delete from room where number = @id";
            MyConnection.ExecuteQuery(sql, new object[] { number });
            return true;
        }
    }
}

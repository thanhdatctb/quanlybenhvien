﻿using QuanLyBenhVien.Controller;
using QuanLyBenhVien.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyBenhVien.View
{
    /// <summary>
    /// Interaction logic for LogIn.xaml
    /// </summary>
    public partial class LogIn : Window
    {
        private AdminController adminController = new AdminController();
        private DoctorController doctorController = new DoctorController();
        private PatientController patientController = new PatientController();
        public LogIn()
        {
            InitializeComponent();
        }

        private void btnLogIn_Click_1(object sender, RoutedEventArgs e)
        {
            string username = this.txtUserName.Text;
            string pasword = this.txtPasswordBox.Password;
            try
            {
                if(adminController.LogIn(username,pasword))
                {
                    new AdminHome().Show();
                }   
                else if(patientController.LogIn(username, pasword))
                {

                }   
                else if(doctorController.LogIn(username,pasword))
                {
                    
                    new DoctorMainForm(MySession.doctor).Show();
                }   
                else
                {
                    MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng");
                }                    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

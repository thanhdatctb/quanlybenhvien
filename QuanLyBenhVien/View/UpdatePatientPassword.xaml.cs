﻿using QuanLyBenhVien.Controller;
using QuanLyBenhVien.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyBenhVien.View
{
    /// <summary>
    /// Interaction logic for UpdatePatientPassword.xaml
    /// </summary>
    public partial class UpdatePatientPassword : Window
    {
        private Patient inputPatient;
        private PatientController patientController = new PatientController();
        public UpdatePatientPassword(Patient patient)
        {
            InitializeComponent();
            this.inputPatient = patient;
            this.txtId.Text = patient.id.ToString();
            this.txtName.Text = patient.name;
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            string hashpass = PasswordController.Sha1Hash(this.txtPass.Password);
            string hashConfrim = PasswordController.Sha1Hash(this.txtConfirm.Password);
            if(hashConfrim != hashpass)
            {
                MessageBox.Show("Mật khẩu không khớp");
                return;
            }
            try
            {
                this.inputPatient.hashpass = hashpass;
                ;
                if (this.patientController.UpdatePassword(inputPatient))
                {
                    MessageBox.Show("Cập nhật thành công");
                    this.Close();
                    if(Session.MySession.admin != null)
                    {
                        AdminHome adminHome = new AdminHome();
                        adminHome.tcAdmin.SelectedIndex = 2;
                        adminHome.Show();
                    }    
                }    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            

        }
    }
}

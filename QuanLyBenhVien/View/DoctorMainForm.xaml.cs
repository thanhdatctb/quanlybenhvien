﻿using QuanLyBenhVien.Controller;
using QuanLyBenhVien.Model;
using QuanLyBenhVien.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyBenhVien.View
{
    /// <summary>
    /// Interaction logic for DoctorMainForm.xaml
    /// </summary>
    public partial class DoctorMainForm : Window
    {
        private Doctor doctor;
        private RoomController roomController = new RoomController();
        private PatientController patientController = new PatientController();
        public DoctorMainForm(Doctor doctor)
        {
            this.doctor = doctor;
            InitializeComponent();
            LoadDoctor();
            LoadRoom();
            LoadResume();
            LoadPatient();
        }
        private void LoadDoctor()
        {
            this.txtDoctorId.Text = doctor.id.ToString();
            this.txtDoctorName.Text = doctor.name;
            this.txtDoctorRole.Text = doctor.role;
            this.txtDoctorMajor.Text = doctor.major;
            this.txtDoctorUserName.Text = doctor.username;
        }
        private void LoadRoom()
        {
            this.dtgvRoom.ItemsSource = roomController.GetAll();
        }
        private void LoadResume()
        {
            string sql = @"select resume.id, resume.doctorId, doctor.name as DoctorName, resume.patientId, patient.name as PatientName, resume.roomId, resume.meetingDate
                            from resume
                            join doctor on resume.doctorId = doctor.id
                            join patient on resume.patientId = patient.id where resume.doctorId = "+MySession.doctor.id;
            this.dtgvResume.ItemsSource = MyConnection.ExecuteQuery(sql).DefaultView;
            //this.dtgvResume.ItemsSource = this.resumeController.GetAll();
        }
        private void LoadPatient()
        {
            this.dtgvPatient.ItemsSource = this.patientController.GetByDoctorId(MySession.doctor.id);
        }
        private void btnDeleteDoctor_Copy_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            new UpdateDoctorPassword(MySession.doctor).Show();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnDeleteRoom_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAddResume_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

﻿using QuanLyBenhVien.Controller;
using QuanLyBenhVien.Model;
using QuanLyBenhVien.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyBenhVien.View
{
    /// <summary>
    /// Interaction logic for UpdateDoctorPassword.xaml
    /// </summary>
    public partial class UpdateDoctorPassword : Window
    {
        private DoctorController doctorController = new DoctorController();
        private Doctor inputDoctor;
        public UpdateDoctorPassword(Doctor doctor)
        {
            InitializeComponent();
            this.inputDoctor = doctor;
            this.txtId.Text = doctor.id.ToString();
            this.txtName.Text = doctor.name;
        }

        private void txtSubmit_Click(object sender, RoutedEventArgs e)
        {
            string hashpass = PasswordController.Sha1Hash(this.txtPass.Password);
            string hashconfirm = PasswordController.Sha1Hash(this.txtConfirm.Password);
            if (hashconfirm != hashpass)
            {
                MessageBox.Show("Mật khẩu không khớp");
                return;
            }
            this.inputDoctor.hashpass = hashpass;
            try
            {
                if(this.doctorController.UpdatePassword(inputDoctor))
                {
                    MessageBox.Show("Cập nhật mật khẩu thành công");
                    if(Session.MySession.admin != null)
                    {
                        this.Close();
                        AdminHome adminHome = new AdminHome();
                        adminHome.tcAdmin.SelectedIndex = 1 ;
                        adminHome.Show();
                    }  
                    else
                    {
                        DoctorMainForm doctorMainForm = new DoctorMainForm(MySession.doctor);
                        doctorMainForm.tcDoctor.SelectedIndex = 1;
                        doctorMainForm.Show();
                    }    
                }    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

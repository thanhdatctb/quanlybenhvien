﻿using QuanLyBenhVien.Controller;
using QuanLyBenhVien.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyBenhVien.View
{
    /// <summary>
    /// Interaction logic for AdminHome.xaml
    /// </summary>
    public partial class AdminHome : Window
    {
        private RoomController roomController = new RoomController();
        private DoctorController doctorController = new DoctorController();
        private PatientController patientController = new PatientController();
        private ResumeController resumeController = new ResumeController();
        public AdminHome()
        {
            InitializeComponent();
            LoadRoom();
            LoadDoctor();
            LoadPatient();
            LoadResume();
        }
        private void LoadRoom()
        {
            List<Room> rooms = this.roomController.GetAll();
            this.dtgvRoom.ItemsSource = rooms;
            this.cbRoom.ItemsSource = rooms;
            this.cbRoom.DisplayMemberPath = "number";
            this.cbRoom.SelectedValuePath = "number";
        }
        private void LoadDoctor()
        {
            List<Doctor> doctors = this.doctorController.GetAll();
            this.dtgvDoctor.ItemsSource = doctors;
            this.cbDoctor.ItemsSource = doctors;
            this.cbDoctor.DisplayMemberPath = "name";
            this.cbDoctor.SelectedValuePath = "id";
        }
        private void LoadPatient()
        {
            List<Patient> patients = this.patientController.GetAll();
            this.dtgvPatient.ItemsSource = patients;
            this.cbPatient.ItemsSource = patients;
            this.cbPatient.DisplayMemberPath = "name";
            this.cbPatient.SelectedValuePath = "id";
        }
        private void LoadResume()
        {
            string sql = @"select resume.id, resume.doctorId, doctor.name as DoctorName, resume.patientId, patient.name as PatientName, resume.roomId, resume.meetingDate
                            from resume
                            join doctor on resume.doctorId = doctor.id
                            join patient on resume.patientId = patient.id";
            this.dtgvResume.ItemsSource = MyConnection.ExecuteQuery(sql).DefaultView;
            //this.dtgvResume.ItemsSource = this.resumeController.GetAll();
        }
        private Room GetRoom()
        {
            int number = int.Parse(this.txtRoomNumber.Text);
            string name = this.txtRoomName.Text;
            int numberOfBed = int.Parse(this.txtNumberOfBeds.Text);
            return new Room(number, name, numberOfBed);
        }
        private Doctor GetDoctor()
        {
            int id = int.Parse(this.txtDoctorId.Text);
            string name = this.txtDoctorName.Text;
            string role = this.txtDoctorRole.Text;
            string major = this.txtDoctorMajor.Text;
            string username = this.txtDoctorUserName.Text;
            return new Doctor(name, role, major, username, null, id);
        }
        private Patient GetPatient()
        {
            int id = int.Parse(this.txtPatientID.Text);
            string name = this.txtPatientName.Text;
            int age = int.Parse(this.txtPatientAge.Text);
            string tell = this.txtPatientTell.Text;
            string username = this.txtPatientUsername.Text;
            Patient patient = new Patient(name, age, tell, 1, username, null, id);
            return patient;
        }
        private Resume GetResume()
        {
            int id = int.Parse(this.txtResumeId.Text);
            int RoomID = (int) this.cbRoom.SelectedValue;
            int DoctorId = (int)this.cbDoctor.SelectedValue;
            int PatientId = (int)this.cbPatient.SelectedValue;
            DateTime meetingDate = (DateTime)this.dpMeetingDate.SelectedDate;
            return new Resume(id, PatientId, RoomID, DoctorId, meetingDate);
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try 
            {
                Room room = GetRoom();
                if(this.roomController.Add(room))
                {
                    LoadRoom();
                    MessageBox.Show("Thêm phòng bệnh thành công"); 
                }    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Room room = GetRoom();
                if (this.roomController.Edit(room))
                {
                    LoadRoom();
                    MessageBox.Show("Sửa phòng bệnh thành công");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDeleteRoom_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Room room = GetRoom();
                if (this.roomController.Delete(room.number))
                {
                    LoadRoom();
                    MessageBox.Show("Xóa phòng bệnh thành công");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAddDoctor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = this.txtDoctorName.Text;
                string role = this.txtDoctorRole.Text;
                string major = this.txtDoctorMajor.Text;
                string username = this.txtDoctorUserName.Text;
                Doctor doctor =  new Doctor(name, role, major, username, null);
                if (this.doctorController.Add(doctor))
                {
                    LoadDoctor();
                    MessageBox.Show("Thêm bác sĩ thành công, mật khẩu mặc định: 123");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnEditDoctor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Doctor doctor = GetDoctor();
                if (this.doctorController.Edit(doctor))
                {
                    LoadDoctor();
                    MessageBox.Show("Sửa bác sĩ thành công");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDeleteDoctor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Doctor doctor = GetDoctor();
                if (this.doctorController.Delete(doctor.id))
                {
                    LoadDoctor();
                    MessageBox.Show("Update thành công");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDeleteDoctor_Copy_Click(object sender, RoutedEventArgs e)
        {
            Doctor doctor = GetDoctor();
            this.Close();
            new UpdateDoctorPassword(doctor).Show();
        }

        private void btnAddPatient_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = this.txtPatientName.Text;
                int age = int.Parse(this.txtPatientAge.Text);
                string tell = this.txtPatientTell.Text;
                string username = this.txtPatientUsername.Text;
                Patient patient = new Patient(name, age, tell, 1, username, null);
                if (this.patientController.Add(patient))
                {
                    LoadPatient();
                    MessageBox.Show("Thêm bệnh nhân thành công, mật khẩu mặc định: 123");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEditPatient_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Patient patient = GetPatient();
                if (this.patientController.Edit(patient))
                {
                    LoadPatient();
                    MessageBox.Show("Sửa bệnh nhân thành công");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDeletePatient_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Patient patient = GetPatient();
                if (this.patientController.Delete(patient.id))
                {
                    LoadPatient();
                    MessageBox.Show("Xóa bệnh nhân thành công");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnUpdatePatientPassword_Click(object sender, RoutedEventArgs e)
        {
            Patient patient = GetPatient();
            this.Close();
            new UpdatePatientPassword(patient).Show();
        }

        private void btnAddResume_Click(object sender, RoutedEventArgs e)
        {
            int RoomID = (int)this.cbRoom.SelectedValue;
            int DoctorId = (int)this.cbDoctor.SelectedValue;
            int PatientId = (int)this.cbPatient.SelectedValue;
            DateTime meetingDate = (DateTime)this.dpMeetingDate.SelectedDate;
            Resume resume = new Resume(0, PatientId, RoomID, DoctorId, meetingDate);
            try
            { 
                if(this.resumeController.Add(resume))
                {
                    MessageBox.Show("Thêm hồ sơ thành công");
                    LoadResume();
                }    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {

            Resume resume = GetResume();
            try
            {
                if (this.resumeController.Edit(resume))
                {
                    MessageBox.Show("Sửa hồ sơ thành công");
                    LoadResume();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Resume resume = GetResume();
            try
            {
                if (this.resumeController.Delete(resume.id))
                {
                    MessageBox.Show("Xóa hồ sơ thành công");
                    LoadResume();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

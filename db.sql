﻿use master
go
if (exists(select * from sysdatabases where name = 'QuanLyBenhVien'))
drop database QuanLyBenhVien
go
create database QuanLyBenhVien
go 
use QuanLyBenhVien
go
create table admin
(
	id int primary key identity(1,1),
	userName varchar(100),
	hashpass varchar(100),
	adminName nvarchar(100)
)
go
create table room
(
	number int primary key,
	name nvarchar(100),
	numberOfBed int
)
go
create table doctor
(
	id int primary key identity(1,1),
	username nvarchar(100),
	hashpass nvarchar(100),
	name nvarchar(100),
	role nvarchar(100),
	major nvarchar(100),
	status int default 1
)
go
create table patient
(
	id int primary key identity(1,1),
	username nvarchar(100),
	hashpass nvarchar(100),
	name nvarchar(100),
	age int,
	tell varchar(15),
	status int default 1
)
go
create table resume
(
	id int primary key identity(1,1),
	patientId int foreign key references patient(id),
	roomId int foreign key references room(number),
	doctorId int foreign key references doctor(id),
	meetingDate DateTime,
)

go
insert into room values 
(101, N'Phòng hồi sức',6),
(102,N'Phòng chăm sóc đặc biệt',6),
(103,N'Phòng chấn thương chỉnh hình',10),
(201, N'Phòng cách ly', 8),
(202,N'Phòng dịch vụ',8)
go
--pass: admin
insert into patient (name, age, tell, status, username, hashpass) values
(N'Nguyễn Văn A', 25, '0912345678',1, 'anv', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
(N'Trần Văn B ', 30, '0915312154',1,'btv', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
(N'Lê Văn Công', 75, '0915312234',1,'conglv', 'd033e22ae348aeb5660fc2140aec35850c4da997')
go
insert into doctor (name, role, major, username, hashpass) values
(N'Trần Văn Thành', N'Chuyên Viên', N'Dinh dưỡng','thanhtv','d033e22ae348aeb5660fc2140aec35850c4da997'),
(N'Lê Bảo Minh', N'Chuyên Viên', N'Chấn thương', 'minhlb', 'd033e22ae348aeb5660fc2140aec35850c4da997')
go
insert into resume (patientId, roomId, doctorId, meetingDate) values
(1,101,1, '12-12-2020'),
(2,102,2,'11-11-2020'),
(3,201,1,'10-10-2020')
go
---username: admin, password: admin, hash: sha1
insert into admin (userName,hashpass,adminName)values 
( 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Admin'),
('admin2', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Admin2')
go
select resume.id, resume.doctorId, doctor.name as DoctorName, resume.patientId, patient.name as PatientName, resume.roomId, resume.meetingDate
from resume
join doctor on resume.doctorId = doctor.id
join patient on resume.patientId = patient.id

select * from patient where id in(
select patient.id
from resume
join doctor on resume.doctorId = doctor.id
join patient on resume.patientId = patient.id
where doctorId = 1)